#ifndef CONFIG_H
#define CONFIG_H

// Uncomment it if you want to see tiles boundaries
//#define DEBUG_SHOW_TILE_BORDER

// Uncomment for benchmarking tile rendering performance
//#define DEBUG_TILE_BENCHMARK

// Uncomment if you want more verbose application output
//#define DEBUG_VERBOSE

#endif // CONFIG_H
